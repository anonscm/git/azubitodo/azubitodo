#!/bin/bash
pdftotext -layout $1 fi
#löschen der überschriften
sed '/Seite [0-9]* von/,/3/d' fi > fitemp
sed '/Anlage 2 T/,/3/d' fitemp > fi
#unterteilung awe/sys
sed -n '/2. Fach/,/$$/p' fi > sys 
sed '/2. Fach/,$d' fi > fitemp 

sed -n '/Abschnitt III/,/$$/p' fitemp > awe 
sed '/Abschnitt III/,$d' fitemp > fi 

sed '/Abschnitt III/,/3/d' awe > awetemp
sed '/2. Fachrichtung/,/3/d' sys > systemp
#semikolons einfügen
sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{33\}\)/\1!/' fi > fi.csv 
sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{33\}\)/\1!/' awetemp > awe.csv
sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{33\}\)/\1!/' systemp > sys.csv

#semikolons aus wörtern hinter wörter verschieben(!)
sed -r 's/(\w)!(\w+\b)/\1\2!/g' fi.csv > fitemp.csv

#leerzeichen entfernen
sed 's/ \+/ /g' fitemp.csv > fi.csv

#gemeinsame und jeweilige inhalte zusammenfügen
cat fi.csv sys.csv > fs.csv
cat fi.csv awe.csv > fa.csv

#leerzeilen entfernen
sed '/^\s*$/d' fa.csv > fiawe.csv
sed '/^\s*$/d' fs.csv > fisys.csv

rm fi fitemp sys awe awetemp systemp awe.csv sys.csv fi.csv fitemp.csv fa.csv fs.csv

./csvkomp.sh fiawe.csv fisys.csv
rm fiawe.csv fisys.csv
