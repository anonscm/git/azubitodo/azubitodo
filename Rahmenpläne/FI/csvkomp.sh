#!/bin/bash

for Eingang in "$@"
do
	start=$(date +"%r")

	nextFile=$(echo $Eingang | sed 's/\..*//')
	
	if [ -e $nextFile'_2'.csv ]
	then
		rm $nextFile'_2'.csv
		touch $nextFile'_2'.csv
	else
		touch $nextFile'_2'.csv
	fi
	if [ -e $nextFile.html ]
	then
		rm $nextFile.html
		touch $nextFile.html
	else
		touch $nextFile.html
	fi
	
	newline=""
	new1=""
	new2=""
	new3=""
	total=0

	while read line
	do

		if [ $(echo $line | cut -d '!' -f 1) ]
		then
			if [ $total -ne 0 ]
			then
				newline=$new1'!'$new2'!'$new3
				echo $newline >> $nextFile'_2'.csv
			fi		
			new1=""
			new2=""
			new3=""
		fi
		
		new1=$new1$(echo $line | cut -d '!' -f 1)' '
		new2=$new2$(echo $line | cut -d '!' -f 2)' '
		if echo $line | grep -e -$ > /dev/null
		then
			new3=$new3$(echo $line | cut -d '!' -f 3 | sed 's/-$//' | sed 's/^ //')
		else
			new3=$new3$(echo $line | cut -d '!' -f 3 | sed 's/^ //')' '
		fi
		
		((++total))
	done < $nextFile.csv

	col1=""
	col2=""
	col3=""


	echo '<head>' >> $nextFile.html
	echo '	<meta http-equiv="content-type" content="text/html; charset=UTF-8">' >> $nextFile.html
	echo '</head>' >> $nextFile.html
	echo '<body bgcolor="#AAAAAA" style="font-family:Arial;">' >> $nextFile.html
	echo '	<h1 align="center">Abschnitt I: Gemeinsame Ausbildungsinhalte</h1>' >> $nextFile.html
	echo '	<table style="width:950px; font-family:Arial;" align="center" border="1" bgcolor="#EEEEEE" style="empty-cells:show">' >> $nextFile.html
	echo '		<tr>' >> $nextFile.html
	echo '			<th style="width:4%">Lfd Nr.</th>' >> $nextFile.html
	echo '			<th style="width:15%">Teil des Ausbildungsberufsbildes</th>' >> $nextFile.html
	echo '			<th>Fertigkeiten und Kenntnisse, die unter Einbeziehung selbständigen Planens, Durchführens und Kontrollierens zu vermitteln sind</th>' >> $nextFile.html
	echo '		</tr>' >> $nextFile.html

	nope=0
	nope2=0
	col1list=""
	
	while read line
	do
		col1=$(echo $line | cut -d '!' -f 1)
		col2=$(echo $line | cut -d '!' -f 2)
		col3=$(echo $line | cut -d '!' -f 3)
		
		if [ $col1 = "6" ] && [ $nope -eq 0 ]
		then
			echo '	</table>' >> $nextFile.html
			echo '		<h1 align="center">Abschnitt II: Berufsspezifische Ausbildungsinhalte</h1>' >> $nextFile.html
			echo '	<table style="width:950px; font-family:Arial;" align="center" border="1" bgcolor="#EEEEEE" style="empty-cells:show">' >> $nextFile.html
			echo '		<tr>' >> $nextFile.html
			echo '			<th style="width:4%">Lfd Nr.</th>' >> $nextFile.html
			echo '			<th style="width:15%">Teil des Ausbildungsberufsbildes</th>' >> $nextFile.html
			echo '			<th>Fertigkeiten und Kenntnisse, die unter Einbeziehung selbständigen Planens, Durchführens und Kontrollierens zu vermitteln sind</th>' >> $nextFile.html
			echo '		</tr>' >> $nextFile.html
			nope=1
		fi
				
		if [ $nope2 -eq 0 ] && [ $nope -eq 1 ]
		then
			for val in $col1list
			do
				if [ $val = $col1 ] && [ $nope2 -eq 0 ]
				then
					echo '	</table>' >> $nextFile.html
					echo '		<h1 align="center">Abschnitt III: Ausbildungsinhalte in den Fachrichtungen</h1>' >> $nextFile.html
					echo '	<table style="width:950px; font-family:Arial;" align="center" border="1" bgcolor="#EEEEEE" style="empty-cells:show">' >> $nextFile.html
					echo '		<tr>' >> $nextFile.html
					echo '			<th style="width:4%">Lfd Nr.</th>' >> $nextFile.html
					echo '			<th style="width:15%">Teil des Ausbildungsberufsbildes</th>' >> $nextFile.html
					echo '			<th>Fertigkeiten und Kenntnisse, die unter Einbeziehung selbständigen Planens, Durchführens und Kontrollierens zu vermitteln sind</th>' >> $nextFile.html
					echo '		</tr>' >> $nextFile.html
					nope2=1
				fi
			done
		fi
		
		col1list=$col1list$col1' '
		
		echo '		<tr>' >> $nextFile.html
		echo '			<td>'$col1'</td>' >> $nextFile.html
		echo '			<td>'$(echo $col2 | sed 's/(§ /<br>&/')'</td>' >> $nextFile.html
		echo '			<td>'$(echo $col3 | sed 's/ [b-z]*)/<br>&/g' | sed 's/ a)/<br>a)/g')'</td>' >> $nextFile.html
		echo '		</tr>' >> $nextFile.html
	done < $nextFile'_2'.csv
	echo '	</table>' >> $nextFile.html
	echo '</body>' >> $nextFile.html

	ende=$(date +"%r")

	echo $start'--> '$ende

done
