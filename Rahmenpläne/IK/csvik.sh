#!/bin/bash
pdftotext -layout $1  iktemp
sed '/Ihr Ansprechpartner\:/,$d' iktemp > ik 

sed -n '/5. Krankenhaus/,/$$/p' ik > krankenhaus 
sed '/5. Krankenhaus/,$d' ik > iktemp

sed -n '/4. Versicherungen/,/$$/p' iktemp > versicherungen
sed '/4. Versicherungen/,$d' iktemp > ik
 
sed -n '/3. Banken/,/$$/p' ik > banken
sed '/3. Banken/,$d' ik > iktemp

sed -n '/2. Handel/,/$$/p' iktemp > handel 
sed '/2. Handel/,$d' iktemp > ik

sed -n '/1. Industrie/,/$$/p' ik > industrie
sed '/1. Industrie/,$d' ik > iktemp

sed '/Seite 12 von/,$d' iktemp > ik

sed '/Seite [0-9]* von/,/3/d' ik > iktemp

sed '/Ausbildungsrahmenplan Info/,/Abschnitt/d' iktemp > ik
sed '/Fertigkeiten und/,/3/d' ik > iktemp


sed '/Seite [0-9]* von/,/3/d' krankenhaus > krankenhaustemp
sed '/5. Krankenhaus/,/3/d' krankenhaustemp > krankenhaus

sed '/4. Versicherungen/,/3/d' versicherungen > versicherungentemp
sed '/4. Versicherungen/,$d' iktemp > ik

sed -n '/3. Banken/,/$$/p' ik > banken
sed '/3. Banken/,$d' ik > iktemp

sed -n '/2. Handel/,/$$/p' iktemp > handel 
sed '/2. Handel/,$d' iktemp > ik

sed -n '/1. Industrie/,/$$/p' ik > industrie
sed '/1. Industrie/,$d' ik > iktemp
sed '/Seite 12 von/,$d' iktemp > ik

sed '/Seite [0-9]* von/,/3/d' ik > iktemp
sed '/Ausbildungsrahmenplan Info/,/Abschnitt/d' iktemp > ik
sed '/Fertigkeiten und/,/3/d' ik > iktemp

sed '/Seite [0-9]* von/,/3/d' krankenhaus > krankenhaustemp
sed '/5. Krankenhaus/,/3/d' krankenhaustemp > krankenhaus

sed '/4. Versicherungen/,/3/d' versicherungen > versicherungentemp

sed '/3. Banken/,/3/d' banken > bankentemp
sed '/Seite 15/,$d' bankentemp > banken

sed '/2. Handel/,/3/d' handel > handeltemp
sed '/Seite 14/,$d' handeltemp > handel

sed '/1. Industrie/,/3/d' industrie > industrietemp
sed '/Seite 13/,$d' industrietemp > industrie


sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{32\}\)/\1!/' iktemp > ik
sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{32\}\)/\1!/' industrie > industrietemp 
sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{32\}\)/\1!/' handel > handeltemp 
sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{32\}\)/\1!/' banken > bankentemp 
sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{32\}\)/\1!/' versicherungentemp > versicherungen
sed -e 's/\(.\{4\}\)/\1!/' -e 's/\(.\{32\}\)/\1!/' krankenhaus > krankenhaustemp 

sed -r 's/(\w)!(\w+\b)/\1\2!/g' ik > iktemp2
sed 's/a)!/!a)/g' iktemp2 > iktemp

cat iktemp industrietemp > industrie.csv
cat iktemp handeltemp > handel.csv
cat iktemp bankentemp > banken.csv
cat iktemp versicherungen > versicherungen.csv
cat iktemp krankenhaustemp > krankenhaus.csv

sed 's/ \+/ /g' industrie.csv > industrietemp.csv
sed 's/ \+/ /g' handel.csv > handeltemp.csv
sed 's/ \+/ /g' banken.csv > bankentemp.csv
sed 's/ \+/ /g' versicherungen.csv > versicherungentemp.csv
sed 's/ \+/ /g' krankenhaus.csv > krankenhaustemp.csv


sed '/^\s*$/d' industrietemp.csv > ik_industrie.csv
sed '/^\s*$/d' handeltemp.csv > ik_handel.csv
sed '/^\s*$/d' bankentemp.csv > ik_banken.csv
sed '/^\s*$/d' versicherungentemp.csv > ik_versicherungen.csv
sed '/^\s*$/d' krankenhaustemp.csv > ik_krankenhaus.csv

rm industrietemp.csv industrie.csv handeltemp.csv handel.csv bankentemp.csv banken.csv versicherungentemp.csv versicherungen.csv krankenhaustemp.csv krankenhaus.csv 
rm industrie industrietemp handel handeltemp banken bankentemp versicherungen versicherungentemp krankenhaus krankenhaustemp 
rm ik iktemp iktemp2


./csvkomp.sh ik_industrie.csv ik_handel.csv ik_banken.csv ik_versicherungen.csv ik_krankenhaus.csv

rm ik_industrie.csv ik_handel.csv ik_banken.csv ik_versicherungen.csv ik_krankenhaus.csv 


